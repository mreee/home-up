<?php include 'head.php'; ?>
      
      <div class="form-with-sidebar">
        <div class="menu-sidebar hidden-xs">
          <h2 class="mls-title">MLS Listing</h2>
          <ul>
            <li><a href="#">Property Basics</a></li>
            <li class="current-menu-item"><a href="#">Room Dimensions</a></li>
            <li><a href="#">Amenities</a></li>
            <li><a href="#">Restrictions</a></li>
            <li><a href="#">Showing Instructions</a></li>
            <li><a href="#">Financing</a></li>
            <li><a href="#">Create E-Signature</a></li>
            <li><a href="#">Agency Relationship</a></li>
            <li><a href="#">MLS Listing Agreement</a></li>
          </ul>
        </div>

        <p class="mls-title visible-xs">MLS Listing</p>
        <div class="lc-form-section lc-form-section-current">
          <h2 class="lc-form-section-title visible-xs"><a href="#">Property Basics</a></h2>
          <h1>Tell us more about your home</h1>
          <form action="">
            <div class="form-group lc-form-group">
              <h3 class="form-group-title">Location</h3>
              <label for="address">Address</label>
              <input type="text" class="form-control lc-form-control" name="address">
              <label for="lname">Last Name</label>
              <input type="text" class="form-control lc-form-control" name="lname">
              <div class="row">
                <div class="col-3 first">
                  <label for="address">City</label>
                  <input type="text" class="form-control lc-form-control" name="address">
                </div>
                <div class="col-3">
                  <label for="other">State</label>
                  <div class="lc-select">
                    <select name="select" class="form-control">
                      <option value="" disabled selected style='display:none;'></option>
                      <option value="value1">Value 1</option> 
                      <option value="value2">Value 2</option>
                      <option value="value3">Value 3</option>
                    </select>
                  </div>
                </div>
                <div class="col-3 last">
                  <label for="lname">Zip code</label>
                  <input type="text" class="form-control lc-form-control" name="lname">
                </div>
              </div> <!-- /.row -->
              <label for="">Cross street(s)</label>
              <input type="text" class="form-control lc-form-control" name="">
              <label for="">Neighborhood</label>
              <input type="text" class="form-control lc-form-control" name="">
              <label for="">Community</label>
              <input type="text" class="form-control lc-form-control" name="">
            </div> <!-- /.lc-form-group -->  
            <div class="form-group lc-form-group">
              <h3 class="form-group-title">House</h3>
              <div class="row">
                <div class="col-2 first">
                  <label for="">Bedrooms</label>
                  <input type="text" class="form-control lc-form-control" name="">
                </div>
                <div class="col-2 last">
                  <label for="">Bathrooms</label>
                  <input type="text" class="form-control lc-form-control" name="">
                </div>
              </div>  <!-- /.row -->
            </div> <!-- /.lc-form-group -->
            <div class="form-group lc-form-group">
              <h3 class="form-group-title">Lot</h3>
              <label for="other">Private Pool</label>
              <div class="lc-select">
                <select name="select" class="form-control">
                  <option value="" disabled selected style='display:none;'></option>
                  <option value="value1">Value 1</option> 
                  <option value="value2">Value 2</option>
                  <option value="value3">Value 3</option>
                </select>
              </div>
              <label for="other">Private SPA</label>
              <div class="lc-select">
                <select name="select" class="form-control">
                  <option value="" disabled selected style='display:none;'></option>
                  <option value="value1">Value 1</option> 
                  <option value="value2">Value 2</option>
                  <option value="value3">Value 3</option>
                </select>
              </div>
              <label for="other">Guest House</label>
              <div class="lc-select">
                <select name="select" class="form-control">
                  <option value="" disabled selected style='display:none;'></option>
                  <option value="value1">Value 1</option> 
                  <option value="value2">Value 2</option>
                  <option value="value3">Value 3</option>
                </select>
              </div>
            </div> <!-- /.lc-form-group -->
            <div class="form-group lc-form-group">
              <h3 class="form-group-title">Radio Inputs</h3>
              <div class="radio">
                <label><input type="radio" name="optradio">Option 1</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="optradio">Option 2</label>
              </div>
              <div class="radio disabled">
                <label><input type="radio" name="optradio" disabled>Option 3</label>
              </div>
            </div> <!-- /.lc-form-group -->
            <div class="form-group lc-form-group">
              <h3 class="form-group-title">Checkbox Inputs</h3>
              <div class="checkbox">
                <label><input type="checkbox" value="">Option 1</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Option 2</label>
              </div>
              <div class="checkbox disabled">
                <label><input type="checkbox" value="" disabled>Option 3</label>
              </div>
            </div> <!-- /.lc-form-group -->
            <div class="form-group lc-form-group lc-form-group-submit row">
              <input type="submit" value="back" class="btn btn-back">
              <input type="submit" value="next" class="btn btn-cta btn-cta-big">
            </div> <!-- /.lc-form-group -->
          </form>
        </div> <!-- /.lc-form-section -->
        
        <div class="lc-form-section">
          <h2 class="lc-form-section-title visible-xs"><a href="#">Room Dimensions</a></h2>
        </div>
        <div class="lc-form-section">
          <h2 class="lc-form-section-title visible-xs"><a href="#">Amenities</a></h2>
        </div>
      </div>  <!-- /.form-with-sidebar -->        
          
<?php include 'foot.php'; ?>