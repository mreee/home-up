<?php include 'head.php'; ?>

      <div class="dashboard-wrap">
        <h1 class="dashboard-title">Your Dashboard</h1>
        <div class="max-width-970-auto row dashboard-top">
          <div class="col-sm-7 gray-bkgd-bdr">
            <h2 class="dark-bkgd-title">Your Home</h2>
            <div class="row">
              <div class="col-xs-7 dashboard-top-image">
                <img src="assets/images/dashboard-house.jpg" alt="">
              </div>
              <div class="col-xs-5 dashboard-top-house-info">
                <h3>3717 Nobel Court<br>San Diego, CA</h3>
                <p>$750,000</p>
                <span class="spacer"></span>
                <div class="text-center">
                  <a class="btn btn-lg btn-primary" href="" role="button">Edit</a>
                </div>
              </div>
            </div>
          </div> 
          <div class="col-sm-4 gray-bkgd-bdr col-sm-offset-1">
            <h2 class="dark-bkgd-title">To Do List</h2>
            <div class="percentage-complete">
              <h3><span>60%</span> complete</h3>
              <div class="percent-scale">
                <div class="percent-bar">
                  <div class="percent-bar-completed" style="width: 37%"></div>
                </div>
              </div>
            </div>
            <div class="text-center to-do-buttons">
              <a class="btn btn-primary" href="" role="button">Add Photos</a>
              <a class="btn btn-primary to-do-complete" href="" role="button">Complete MLS</a>
              <a class="btn btn-primary" href="" role="button">Complete Disclosures</a>
            </div>
          </div>
        </div> <!-- end .dashboard-top -->
        <div class="max-width-970-auto gray-bkgd-bdr dashboard-services">
          <h2 class="dark-bkgd-title">Services</h2>
          <div class="row">
            <div class="col-sm-4 dashboard-service">
              <img src="assets/images/icon-camera.png" alt="">
              <h3>Professional Photographs</h3>
              <a class="btn btn-lg btn-primary" href="" role="button">View Options</a>
            </div>
            <div class="col-sm-4 dashboard-service">
              <img src="assets/images/icon-user-big.png" alt="">
              <h3>Professional Appraisal</h3>
              <a class="btn btn-lg btn-primary" href="" role="button">View Options</a>
            </div>
            <div class="col-sm-4 dashboard-service">
              <img src="assets/images/icon-tools.png" alt="">
              <h3>Marketing Tools</h3>
              <a class="btn btn-lg btn-primary" href="" role="button">View Options</a>
            </div>
          </div>
        </div> <!-- end .dashboard-services -->
        <div class="max-width-970-auto gray-bkgd-bdr dashboard-broker">
          <h2 class="dark-bkgd-title">Meet your broker</h2>
          <div class="row">
            <div class="col-sm-8 broker-bio">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel tincidunt dolor. Etiam a nunc purus. Ut vel eros semper, pulvinar turpis non, mattis metus. Nam mattis est sed ligula scelerisque auctor. Suspendisse aliquam, erat sed fermentum varius, risus lorem aliquet diam, quis ullamcorper risus lacus non enim. Ut augue lacus, pellentesque porttitor gravida sed, interdum eleifend nisi. Vestibulum sit amet rhoncus dolor</p>
              <div class="broker-info">
                <img src="assets/images/signature.png" alt="signature">
                <p class="bold">Jordy Nelson</p>
                <p>Broker</p>
              </div>
            </div>
            <img src="assets/images/broker.png" alt="" class="broker-image">
          </div>
          <div class="broker-contact-info">
            <span class="broker-phone"><a href="tel:8888675309">(888) 867-5309</a></span>
            <span class="broker-email"><a href="mailto:broker@homevana.com">broker@homevana.com</a></span>
          </div>
        </div> <!-- end .dashboard-broker -->
      </div> <!-- end .dashboard-wrap -->
          
<?php include 'foot.php'; ?>