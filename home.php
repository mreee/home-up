<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="//use.typekit.net/get1rwy.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>
   
    <title>Home Up</title>

    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="container">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php"><img src="assets/images/logo.png" alt="homevana"></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
            <li><a href="#">How it works</a></li>
            <li class="active"><a href="#">Testimonials</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Contact</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="dashboard.php">Denny</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>

      <div class="home-hero" style="overflow:hidden;">
        <div class="home-hero-content">
          <h1>The new way <span>to sell your home</span></h1>
          <div class="text-center"> 
            <a href="#" class="btn btn-cta">Get Started</a>
          </div>
        </div>
        <ul class="home-hero-list">
          <li>Pay ZERO Listing Commission</li>
          <li>Property listed in your local MLS</li>
          <li>Featured on Zillow, Trulia, and Realtor.com</li>
          <li>Yes, 100% FREE</li>
        </ul>
      </div> <!-- end .home-hero -->
      <div class="home-content-wrap">
        <h3 class="tab-title tab-title-negative">How it works</h3>
        <div class="row home-content-section home-content-section-1">
          <h2><span>1</span> Register your home</h2>
          <img src="assets/images/home-illustration-1.png" alt="">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget odio et dolor posuere pellentesque sit amet vel magna. Praesent bibendum tincidunt rhoncus. Ut dapibus ligula vitae metus tempor volutpat. Mauris consequat tristique finibus. Pellentesque in lacus condimentum, aliquet ligula in, fringilla arcu.</p>
        </div> <!-- end .home-content-section -->
        <img src="assets/images/home-lines-1.png" alt="" class="home-lines">
        <div class="row home-content-section home-content-section-2">
          <h2><span>2</span> Enter MLS Information</h2>
          <img src="assets/images/home-illustration-2.png" alt="">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget odio et dolor posuere pellentesque sit amet vel magna. Praesent bibendum tincidunt rhoncus. Ut dapibus ligula vitae metus tempor volutpat. Mauris consequat tristique finibus. Pellentesque in lacus condimentum, aliquet ligula in, fringilla arcu.</p>
        </div> <!-- end .home-content-section -->
        <img src="assets/images/home-lines-2.png" alt="" class="home-lines">
        <div class="row home-content-section home-content-section-3">
          <h2><span>3</span> Sell your home</h2>
          <img src="assets/images/home-illustration-3.png" alt="">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget odio et dolor posuere pellentesque sit amet vel magna. Praesent bibendum tincidunt rhoncus. Ut dapibus ligula vitae metus tempor volutpat. Mauris consequat tristique finibus. Pellentesque in lacus condimentum, aliquet ligula in, fringilla arcu.</p>
        </div> <!-- end .home-content-section -->
        <div class="home-center-cta">
          <a href="#" class="btn btn-cta">Get Started</a>
        </div>

        <h3 class="tab-title tab-title-testimonials">Testimonials</h3>
        <div id="carousel-home" class="carousel slide-home" data-ride="carousel">
          <div class="carousel-inner">
            <div class="item active">
              <img src="assets/images/home-slider.jpg" alt="...">
              <div class="carousel-content">
                <p>"This is the best site for selling property by owners, period! The amazing and the best thing about Homevana is all these services I received were FREE. I received three offers all above my asking price from an open house. I am very pleased and happy that I discovered and used Homevana."
                  <span>- Mary Clarkson</span>
                </p>
              </div>
            </div>
            <div class="item">
              <img src="assets/images/home-slider.jpg" alt="...">
              <div class="carousel-content">
                <p>"This is the best site for selling property by owners, period! The amazing and the best thing about Homevana is all these services I received were FREE. I received three offers all above my asking price from an open house. I am very pleased and happy that I discovered and used Homevana."
                  <span>- Kenny Loggins</span>
                </p>
              </div>
            </div>
            <div class="item">
              <img src="assets/images/home-slider.jpg" alt="...">
              <div class="carousel-content">
                <p>"This is the best site for selling property by owners, period! The amazing and the best thing about Homevana is all these services I received were FREE. I received three offers all above my asking price from an open house. I am very pleased and happy that I discovered and used Homevana."
                  <span>- Brian Ogden</span>
                </p>
              </div>
            </div>
          </div>
          <!-- Indicators -->
          <ol class="carousel-indicators carousel-indicators-home">
            <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-home" data-slide-to="1"></li>
            <li data-target="#carousel-home" data-slide-to="2"></li>
          </ol>
        </div> <!-- end .carousel -->
        <img src="assets/images/home-lines-3.png" alt="" class="home-lines home-lines-3">
        <h3 class="tab-title tab-title-center">We do it all for free</h3>
        <div class="comparison-table">
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th></th>
                  <th class="td-blue">Homeup</th>
                  <th>ForSaleByOwner<br>.com</th>
                  <th>Redfin<br>.com</th>
                  <th>Traditional<br>Broker</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="text-left">MLS Listing</td>
                  <td class="td-blue"><span class="table-check-white">Free</span></td>
                  <td>$699</td>
                  <td>$4,500</td>
                  <td>$18,000</td>
                </tr>
                <tr>
                  <td class="text-left">Listing posted on<br>company website</td>
                  <td class="td-blue"><span class="table-check-white">Free</span></td>
                  <td>$81/month</td>
                  <td><span class="table-check"></span></td>
                  <td><span class="table-check"></span></td>
                </tr>
                <tr>
                  <td class="text-left">Your property featured on<br> Zillow, Trulia &amp; Realtor.com</td>
                  <td class="td-blue"><span class="table-check-white">Free</span></td>
                  <td><span class="table-x"></span></td>
                  <td><span class="table-x"></span></td>
                  <td><span class="table-x"></span></td>
                </tr>
                <tr>
                  <td class="text-left">Complete the entire<br>transaction online</td>
                  <td class="td-blue"><span class="table-check-white">Free</span></td>
                  <td><span class="table-x"></span></td>
                  <td><span class="table-x"></span></td>
                  <td><span class="table-x"></span></td>
                </tr>
                <tr>
                  <td class="text-left">Online step by step<br>instructions</td>
                  <td class="td-blue"><span class="table-check-white">Free</span></td>
                  <td><span class="table-x"></span></td>
                  <td><span class="table-x"></span></td>
                  <td><span class="table-x"></span></td>
                </tr>
                <tr>
                  <td class="text-left">Paperwork auto-generated<br>for you</td>
                  <td class="td-blue"><span class="table-check-white">Free</span></td>
                  <td><span class="table-x"></span></td>
                  <td><span class="table-x"></span></td>
                  <td><span class="table-x"></span></td>
                </tr>
              </tbody>
            </table>
          </div> <!-- end .responsive-table -->
        </div> <!-- end .comparison-table -->
        <div class="home-cta">
          <div class="home-cta-content">
            <h1>Sell your home<span>today</span></h1>
            <div class="home-cta-arrow">
              <a href="#" class="btn btn-cta">Get Started</a>
            </div>
          </div>
        </div>
      </div> <!-- end .home-content-wrap -->
      

<?php include 'foot.php'; ?>