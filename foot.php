      <footer>
        <div class="row footer-content">
          <div class="col-sm-4">
            <a href="home.php"><img src="assets/images/logo-yellow.png" alt=""></a>
            <div class="sm-icons">
              <a href="https://facebook.com" target="_blank"></a>
              <a href="https://twitter.com" target="_blank"></a>
              <a href="https://youtube.com" target="_blank"></a>
              <a href="https://pinterest.com" target="_blank"></a>
              <a href="https://linkedin.com" target="_blank"></a>
              <a href="https://plus.google.com" target="_blank"></a>
            </div>
          </div>
          <div class="col-sm-4">
            <ul class="footer-menu">
              <li><a href="#">How it works</a></li>
              <li><a href="#">Testimonials</a></li>
              <li><a href="#">About</a></li>
              <li><a href="#">FAQ</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>
          <div class="col-sm-4 visible-sm-block visible-md-block visible-lg-block">
            <div class="footer-form">
              <p>Sign up for our newsletter!</p>
              <form action="#">
                <input type="text" placeholder="hello@homeup.com">
                <input type="submit" value="sign up">
              </form>
            </div>
          </div>
        </div>
      </footer>
    </div> <!-- /container -->


      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <script src="assets/javascripts/bootstrap.js"></script>
      <script src="assets/javascripts/scripts.js"></script>

      <script type="text/javascript" data-project="home-up" src="http://localhost:48626/takana.js"></script>
  </body>
</html>